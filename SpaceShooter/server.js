require('dotenv').config();
var express = require('express'),
app = express(),
port = process.env.PORT,
mongoose = require('mongoose'),
Player = require('./api/models/player'),
Enemy = require('./api/models/enemy'),
bodyParser = require('body-parser');


mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_DB);

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var routes = require('./api/routes/playerRoutes');
var enemyRoutes = require('./api/routes/enemyRoutes');
var enemiesRoutes = require('./api/routes/enemiesRoutes');
routes(app);
enemyRoutes(app);
enemiesRoutes(app);

app.listen(port);

console.log(port);

app.use(function(req,res){
    res.status(404).send({url: req.originalUrl + ' not found'});
});