var mongoose = require('mongoose'),
    Player = mongoose.model('Player');

exports.login = function(req,res){
    Player.findOne({name: req.body.name},function(err,player){
        if(err){res.send(err);}
             

        if (player.validatePassword(req.body.password)) {
            //res.send('Ok');
            var tok = player.generateAuthToken();
            res.json({token: tok});
        }
        else res.send('Wrong password');
    });
};  

exports.register = function(req,res){
    var new_player = new Player(req.body);
    new_player.setPassword(req.body.password);
    new_player.save(function(err,player){
        if(err) res.send(err);
        res.json("Player successfully registered");
    });    
};

exports.listPlayers = function(req,res){
    Player.find({},function(err,player){
        if(err) res.send(err);
        res.json(player);
    });
};