'use strict'

var mongoose = require('mongoose'),
    Enemy = mongoose.model('Enemy');

exports.listAllEnemies = function(req,res){
    Enemy.find({},function(err,enemy){
        if(err){
            res.send(err);
        }else{
            res.json(enemy)
        }
    });
};    

exports.addEnemy = function(req,res){
    var new_enemy = new Enemy(req.body);
    new_enemy.save(function(err,enemy){
        if(err){
            res.send(err);
        }else{
            res.json(enemy);
        }
    });
};

exports.getEnemyByName = function(req,res){
    Enemy.findOne({name:req.body.name},function(err,enemy){
        if(err){
            res.send(err);
        }else{
            res.json(enemy);
        }
    });
};

exports.getEnemyById = function(req,res){
    Enemy.findOne({_id:req.body.id},function(err,enemy){
        if(err){
            res.send(err);
        }else{
            res.json(enemy);
        }
    });
};

exports.updateEnemy = function(req,res){
    Enemy.findOneAndUpdate({_id: req.body.id},req.body,{new: true},function(err,enemy){
        if(err){
            res.send(err);
        }else{
            res.json(enemy);
        }
    });
};

exports.deleteEnemy = function(req,res){
    Enemy.findOneAndDelete({_id: req.body.id},function(err,enemy){
        if(err){
            res.send(err);
        }else{
            res.send(enemy.name + " deleted");
        }
    })
}