'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var enemySchema = new Schema({
    name: {
        type: String,
        required: true,
        index: {unique:true}
    },
    health: {
        type: Number,
        required: true
    },
    positionX: {
        type: Number,
        required: true
    },
    positionY: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Enemy',enemySchema);