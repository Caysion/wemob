'use strict'

module.exports = function(app){
    const enemy = require('../controllers/enemyController');
    const tokenAuthenticator = require('../middleware/tokenAuthenticator');

    app.route('/enemy').post(tokenAuthenticator.authenticate,enemy.addEnemy);
    app.route('/enemy/getByName').get(tokenAuthenticator.authenticate,enemy.getEnemyByName);
    app.route('/enemy')
        .get(tokenAuthenticator.authenticate,enemy.getEnemyById)
        .put(tokenAuthenticator.authenticate,enemy.updateEnemy)
        .delete(tokenAuthenticator.authenticate,enemy.deleteEnemy);
}