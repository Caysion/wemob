'use strict'

module.exports = function(app){
    const enemies = require('../controllers/enemyController');
    const tokenAuthenticator = require('../middleware/tokenAuthenticator');
    app.route("/enemies").get(tokenAuthenticator.authenticate,enemies.listAllEnemies);
}