module.exports = function(app){
    var players = require('../controllers/playerController');

    app.route('/players').get(players.listPlayers);
    app.route('/login').post(players.login);
    app.route('/register').post(players.register);
}